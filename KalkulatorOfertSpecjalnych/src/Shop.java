import java.util.Scanner;

public class Shop {

	public static final int EXIT = 0;
	public static final int ADD_PRODUCT = 1;
	public static final int SHOW_PRODUCTS = 2;
	public static final int ENTER_PROMO_CODE = 3;
	
	public static void main(String[] args) {
		
		Scanner Input = new Scanner(System.in);		
		Basket koszyk = new Basket();
		Product[] magazine = new Product[10];
		
		int option = -1;
		int productOption = 0;
		
		magazine[0] = new Product("Frugo", 9999, 3, true, 15);
		magazine[1] = new Product("Woda mineralna gazowana", 9999, 1.5, false, 0);
		magazine[2] = new Product("Baton Mars", 9999, 2.3, false, 0);
		magazine[3] = new Product("Bulki", 9999, 0.8, false, 0);
		magazine[4] = new Product("Pomarancze", 9999, 1, true, 20);
		magazine[5] = new Product("Tymbark", 9999, 2, true, 10);
		magazine[6] = new Product("snickers", 9999, 1.20, false, 0);
		magazine[7] = new Product("Pomidory", 9999, 0.70, false, 0);
		magazine[8] = new Product("Pizza ", 9999, 7.20, true, 10);
		magazine[9] = new Product("Banany", 9999, 1.20, false, 0);
		
		
		while (option != EXIT) {
			
			System.out.println("Witaj! Wlasnie trzymasz koszyk. Co chcesz zrobic?");
			System.out.println("0 - Idz do kasy.");
			System.out.println("1 - Wloz produkt do koszyka.");
			System.out.println("2 - Zobacz co masz w koszyku.");
			System.out.println("3 - Wprowadz kod promocyjny.");
			System.out.println("Wartosc twojego koszyka to " + koszyk.getValue() + " zl.");
			
			option = Input.nextInt();
			Input.nextLine();
			
			switch(option) {
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			case EXIT: 
			System.out.println("Zapraszam do kasy!");
			break;
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			case ADD_PRODUCT:
			
			Product product = new Product();
			
			System.out.println("Wybierz dostepny produkt:");
			
			for(int i=0; i<magazine.length;i++)
			{
				System.out.println(i+1 + ". " + magazine[i].getName() + ", dostepnych sztuk: " + magazine[i].getQuantity() + ", cena: " + magazine[i].getPrice()
						+ "zl, promocja: " + magazine[i].isDiscount() + ", Znizka: " + magazine[i].getDiscountValue() + "%.");
			}
			
			productOption = Input.nextInt();
			Input.nextLine();
		
			System.out.println("Ile sztuk chcesz kupic?");
			
			int pieces = Input.nextInt();
			Input.nextLine();
			
			
				if (productOption>0 && productOption < magazine.length+1)
				{
				productOption--;
				product = new Product(magazine[productOption].getName(), pieces, magazine[productOption].getPrice(), magazine[productOption].isDiscount(), 
						magazine[productOption].getDiscountValue());
				
				int tmp = magazine[productOption].getQuantity() - pieces;
				magazine[productOption].setQuantity(tmp);
				
				} else System.out.println("Nieprawidlowa opcja.");

						
			koszyk.addProduct(product);
			koszyk.countValue();
	
			break;
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			case SHOW_PRODUCTS:
				koszyk.printBusket();
				break;
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			case ENTER_PROMO_CODE:
				
				System.out.println("Po dodaniu produktu, kody znizkowe nalezy wprowadzic jeszcze raz!");
				System.out.println("Wprowadz swoj kod: ");
				koszyk.promoCode(Input.nextLine());
				
			break;
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			default:  System.out.println("cos poszlo nie tak");
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////				
			}
					
		}	
		Input.close();	
	}

}