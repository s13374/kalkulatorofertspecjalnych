public class Product {
	
	private String name;
	private int quantity;
	private double price;
	private double discountValue;
	private boolean discount = false;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public double getDiscountValue() {
		return discountValue;
	}
	public void setDiscountValue(double discountValue) {
		this.discountValue = discountValue;
	}
	public boolean isDiscount() {
		return discount;
	}
	public void setDiscount(boolean discount) {
		this.discount = discount;
	}
	
	public Product(String name, int quantity, double price, boolean discount, double discountValue) {
		setName(name);
		setQuantity(quantity);
		setPrice(price);
		setDiscount(discount);
		setDiscountValue(discountValue);
	}
	
	public Product() {}
	
}